<?php

/**
 * Fonctions utilisable par le squelette fourmis_doc.html
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Obtenir la liste des objets d'un plugin
 *
 * @param  string $prefix
 * @return array
 */
function objets_plugin(?string $prefix=''){
	if (strlen($prefix)){
		// la configuration est classique
		if (include_spip('base/'. $prefix)){
			$declarationfunction = $prefix . '_declarer_tables_objets_sql';
			if (function_exists($declarationfunction)){
				$objets = $declarationfunction([]);
				return array_column($objets,'type');
			}
		// la configuration n'est pas classique, il faut regarder le paquet
		} else {
			$constante = '_DIR_PLUGIN_'.strtoupper($prefix);
			if (defined($constante)) {
				$fichier = constant($constante) . 'paquet.xml';
				if (file_exists($fichier)){
					$xml = simplexml_load_file($fichier);
					// on examine le xml pour y trouver le fichier à inclure
					foreach ($xml->pipeline as $pipe) {
						$attribut = $pipe->attributes();
						if ($attribut['nom'] == 'declarer_tables_objets_sql'){
							if (isset ($attribut['inclure']) 
								and $inclure = $attribut['inclure']
								and strlen($inclure)
								and strpos($inclure,'.php')
								and $inclure = str_ireplace('.php', '', $inclure)
								and include_spip($inclure)
							){
								$declarationfunction = $prefix . '_declarer_tables_objets_sql';
								if (function_exists($declarationfunction)){
									$objets = $declarationfunction([]);
									return array_column($objets,'type');
								} 
							}
						}
					}
				}
			}
		}
	}
	return [];
}