<?php

/**
 * Fonctions utilisable par le squelette bouton_arbo.html
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * est-ce que le fichier ou le répertoire existe
 *
 * @param  string $chemin
 * @return string
 */
function aide_existe(?string $chemin=''){
	if (strlen($chemin) > 7){
		// test de l'existance d'un fichier d'aide
		if (substr($chemin, -5) == '.spip') {
			// Indique si le fichier est un véritable fichier
			return is_file($chemin) ? ' ' : '';
		}
		// Indique si le fichier est un dossier
		return is_dir($chemin) ? ' ' : '';
	} else {
		return '';
	}
 }