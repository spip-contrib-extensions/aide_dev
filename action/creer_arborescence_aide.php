<?php
/**
 * Utilisation de l'action pour créer l'arborescence et les fichiers .spip d'aide
 *
 * @plugin     Aide_dev
 * @copyright  2023
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Aide_dev\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour créer l'arborescence de l'aide
 *
 * utilise l'argument de l'action sécurisée.
 * 
 */
function action_creer_arborescence_aide_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	$arg = explode('-', $arg);
	if (isset($arg[0]) and $prefix = (string) $arg[0] and strlen($prefix)
		and isset($arg[1]) and $lang = (string) $arg[1] and strlen($lang)
	){
		// avoir le chemin
		$constante = '_DIR_PLUGIN_'.strtoupper($prefix);
		if (defined($constante)) {
			// le répertoire aide/
			$path = constant($constante) . 'aide';
			if (!isdir_or_createit($path)){
				include_spip('inc/minipres');
				echo minipres('Erreur', "Il n'a pas été possible de créer le répertoire $path.");
				exit;
			}
			// le repertoire aide/lang
			$path .= '/' . $lang;
			if (!isdir_or_createit($path)){
				include_spip('inc/minipres');
				echo minipres('Erreur', "Il n'a pas été possible de créer le répertoire $path.");
				exit;
			}
			// le groupe
			if (isset($arg[2]) and $groupe = (string) $arg[2] and strlen($groupe)){
				$path .= '/' . $groupe;
				if (!isdir_or_createit($path)){
					include_spip('inc/minipres');
					echo minipres('Erreur', "Il n'a pas été possible de créer le répertoire $path.");
					exit;
				}
				// l'entrée
				if (isset($arg[3]) and $entree = (string) $arg[3] and strlen($entree) and substr($entree, -5) == '.spip' ){
					$path .= '/' . $entree;
					if (!is_file($path)){
						$texte = '';
						// la déclaration de la table relative à l'entrée
						// peut nous permettre de prérédiger un contenu pertinent
						if (isset($arg[4]) and $objet = (string) $arg[4] and strlen($objet)){
						// -- prérédiger l'aide
							include_spip('inc/filtres');
							$table = table_objet_sql($objet);
							$desc = lister_tables_objets_sql($table);
							if (substr($entree, 0, -5) == 'statut'){
								$texte .= aide_statuts($desc, $lang);
							}
							if (substr($entree, 0, -5) == 'titre'){
								// déterminer s'il y a un `surtitre` et un `soustitre`
								if (isset($desc['field']['surtitre']) and isset($desc['field']['soustitre'])){
									$texte .= implode("\n", aide_yaml('titres', $lang));
								} else {
									$texte .= implode("\n", aide_yaml('titre', $lang));
								}
							}
						}
						// -- enregistrer le fichier d'aide
						if (!file_put_contents($path, $texte)){
							include_spip('inc/minipres');
							echo minipres('Erreur', "Il n'a pas été possible de créer le fichier $path.");
							exit;
						}
					}
				}
			}
		}
	}
}


// fonction privée pour créer un répertoire d'aide
function isdir_or_createit($path) {
	if(is_dir($path)) { // Le dossier est-t-il là ?
		return true;
	} else {
		if(mkdir($path)) { // Crée un dossier.
			return true;
		} else {
			return false; // informer que quelque chose ne va pas.
		}
	}
}

// fonction privée pour créer un répertoire d'aide
// todo : - icone lorsque l'aide le permettra
//        - pouvoir verifier s'il n'y a pas un Yaml pour un objet particulier
function aide_statuts(array $desc,string $lang) {
	$tableau ='';
	// -- charger les informations sur les statuts connus
	$infos = aide_yaml('statuts', $lang);
	$tableau = $infos['intro'];
	// -- créer et nourrir un tableau des statuts
	if (isset($desc['statut_textes_instituer'])) {
		$tableau .= '||' . $infos['titre_statuts'] . "||\n";
		$tableau .= '|' . _T($desc['texte_changer_statut']) . '|' . $infos['colonne_explication'] . " |\n";
		$statuts = $desc['statut_textes_instituer'];
		foreach ($statuts as $cle => $statut) {
			$tableau .= '|' . _T($statut) . '|' . $infos[$cle] . "|\n";
		}
	}
	$tableau .= $infos['note'];
	return $tableau;
}

function aide_yaml(string $entree,string $lang) {
	include_spip('inc/yaml');
	$chemin = _DIR_PLUGIN_AIDE_DEV . "aide_entrees/{$lang}/{$entree}.yaml";
	return yaml_decode_file($chemin);
}