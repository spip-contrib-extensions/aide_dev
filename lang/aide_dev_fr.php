<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [
	'titre_choix_plugin' => 'Choisissez un plugin',
	'titre_aide_dev' => 'Assistance au développement d’une aide pour les objets de votre plugin',
	'info_pipeline_aide_index' => 'Fichier <code>@fichier@_pipelines.php</code>',
	'info_idiome_aide' => 'Fichier <code>lang/aide_@lang@.php</code>',
	'info_aborescense_aide' => 'Arborescence <code>aide/@lang@/..</code>',
	'info_paquet' => 'Fichier <code>paquet.xml</code>',
	'info_aucun_objet' => 'Le plugin n’a pas d’objet',
	'info_aide_objet' => 'L’objet @objet@',
	'info_aide_contenu' => 'Contenu',
	'info_aide_statut' => 'Statut',
	'menu_aide_dev' => 'Assistance création d’Aide',
	'icone_creer_rep' => 'Créer le répertoire du groupe',
	'icone_creer_fichier' => 'Créer le fichier de l’entrée',
	'confirm_creer_rep' => 'Êtes vous sûr de vouloir créer ce répertoire d’aide ?',
	'confirm_creer_fichier' => 'Êtes vous sûr de vouloir créer ce fichier d’aide ?',
	'info_action' => 'Action proposée',
	'info_rep' => 'Répertoire',
	'info_fichier' => 'Fichier',
	'info_aide_texte' => 'Texte',
	'info_aide_titre' => 'Titre',
];