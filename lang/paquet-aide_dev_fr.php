<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	'aide_dev_description' => 'Ce plugin a pour but de faciliter la création d’une aide pour vos plugins.',
	'aide_dev_nom' => 'Aide_dev',
	'aide_dev_slogan' => 'De l’aide pour l’aide...'
];
